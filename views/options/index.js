import {
    getFlatFolderList,
    genericFoldersAreEqual,
    genericFolderToLocalFolder
} from '../../modules/core-utils.mjs.js';
import {
    clearContents,
    createRemindMeBeforePicker,
    createTimePresetPicker,
    selectDefaultActionPreset,
    selectDefaultTimePreset,
    translateDocument,
    webHandler
} from '../../modules/ui-utils.mjs.js';
import { createLogger } from '../../modules/logger.mjs.js';

const logger = createLogger('views/options');

let uiLocked = false;

const createFolderPicker = async (element, account) => {
    // 

    const nullValue = document.createElement('option');
    nullValue.value = 'null';
    nullValue.innerText = browser.i18n.getMessage(
        'view.options.default-icebox-folder.option-none'
    );
    element.appendChild(nullValue);

    const allFolders = await getFlatFolderList(account);

    allFolders.forEach(accountOrFolder => {
        if (accountOrFolder.type === 'account') {
            const optionElement = document.createElement('option');
            optionElement.innerText = accountOrFolder.name;
            optionElement.disabled = true;
            element.appendChild(optionElement);
        } else {
            const folderOptionElement = document.createElement('option');
            const spacing = new Array(accountOrFolder.depth * 2).join(' ');

            folderOptionElement.style.paddingLeft = `${20 *
                (accountOrFolder.depth + 1)}px`;
            folderOptionElement.innerText = `${spacing}${accountOrFolder.name}`;
            folderOptionElement.value = JSON.stringify(accountOrFolder.folder);
            element.appendChild(folderOptionElement);
        }
    });

    return element;
};

const selectDefaultIceboxFolder = (selectElement, settingsValue) => {
    const availableOptions = Array.from(selectElement.options)
        .map(item =>
            item.disabled
                ? undefined
                : {
                      index: item.index,
                      value: JSON.parse(item.value)
                  }
        )
        .filter(
            item =>
                item &&
                (item.value === settingsValue ||
                    (item.value &&
                        settingsValue &&
                        genericFoldersAreEqual(item.value, settingsValue)))
        )
        .map(({ index }) => index);

    const selectedIndex = availableOptions.shift() || 0;

    selectElement.selectedIndex = selectedIndex;
};

const lockUI = locked => {
    const lockElement = document.getElementById('mailmindr-ui-lock');
    if (locked) {
        document.body.style.overflowX = 'hidden';
        document.body.style.overflowY = 'hidden';
        lockElement.style.display = 'block';
    } else {
        lockElement.style.display = 'none';
        document.body.style.overflowX = 'auto';
        document.body.style.overflowY = 'auto';
    }

    Array.from(document.querySelectorAll('fieldset')).forEach(
        fieldsetElement => (fieldsetElement.disabled = locked)
    );

    uiLocked = locked;

    return locked;
};

const openTimespanEditor = async (port, timespan) => {
    await port.postMessage({
        action: 'show:time-preset-editor',
        payload: { preset: timespan }
    });
};

const displayTimePresets = timePresetList => {
    const timePresets = document.getElementById(
        'mailmindr-options_time-preset-list'
    );

    // 
    clearContents(timePresets);

    timePresetList.forEach(timePreset => {
        if (!timePreset.isSelectable) {
            return;
        }

        const timeOption = document.createElement('option');
        if (timePreset.isGenerated) {
            timeOption.disabled = true;
        }
        timeOption.className = 'mailmindr-list-item';
        timeOption.innerText = timePreset.text;
        timeOption.value = JSON.stringify(timePreset);
        timePresets.appendChild(timeOption);
    });
};

const createActionPresetPicker = (element, actionPresetList) => {
    Array.from(element.children).forEach(item => item.remove());

    actionPresetList.map(actionPreset => {
        const actionOption = document.createElement('option');
        actionOption.value = JSON.stringify(actionPreset);
        actionOption.innerText = actionPreset.text;

        element.appendChild(actionOption);
    });

    return element;
};

const doCancelTimespanEditor = () => {
    lockUI(false);
};

const displaySettings = async options => {
    const { presets, settings } = options;
    const { time, actions, remindMeMinutesBefore } = presets;

    displayTimePresets(time);

    // 
    const {
        defaultTimePreset,
        defaultActionPreset,
        defaultIceboxFolder,
        defaultRemindMeMinutesBefore,
        snoozeTime
    } = settings;

    /** @type {HTMLInputElement} */ (document.getElementById(
        'mailmindr-options_snooze-time'
    )).value = snoozeTime;

    const defaultTimePresetPicker = createTimePresetPicker(
        document.getElementById('mailmindr-options_default-time-preset'),
        time
    );

    selectDefaultTimePreset(defaultTimePresetPicker, defaultTimePreset);

    const defaultActionPresetPicker = createActionPresetPicker(
        document.getElementById('mailmindr-options_default-action-preset'),
        actions
    );

    selectDefaultActionPreset(defaultActionPresetPicker, defaultActionPreset);

    const defaultRemindMeBeforePicker = document.getElementById(
        'mailmindr-options_default-reminder-preset'
    );
    createRemindMeBeforePicker(
        document,
        defaultRemindMeBeforePicker,
        remindMeMinutesBefore,
        defaultRemindMeMinutesBefore
    );

    // 
    // 
    // 
    // 

    const accounts = await browser.accounts.list(false);

    for (let account of accounts) {
        const identity = account.identities.at(0);
        const accountIdentity = identity?.email || 'localFolders';

        const pickedIceBox = settings.iceboxFolders.find(
            item => item.accountIdentity === accountIdentity
        );

        const iceBoxPicker = findIceboxPickerForIdentity(accountIdentity);

        if (iceBoxPicker && pickedIceBox && pickedIceBox.folder) {
            selectDefaultIceboxFolder(iceBoxPicker, pickedIceBox.folder);
        }
    }

    const defaultIceBoxPicker = findIceboxPickerForIdentity('default');
    if (defaultIceBoxPicker && defaultIceboxFolder) {
        selectDefaultIceboxFolder(
            defaultIceBoxPicker,
            defaultIceboxFolder.folder
        );
    }
};

const onLockActionButtonClick = async (port, event) => {
    const { target } = event;
    if (target) {
        const { value: action } = target;
        switch (action) {
            case 'bring-to-front': {
                await port.postMessage({
                    action: 'dialog:bring-to-front',
                    payload: { dialogType: 'mailmindr:time-preset-editor' }
                });
                break;
            }
            case 'proceed-anyways': {
                await port.postMessage({ action: 'settings:force-unlock' });
                break;
            }
        }
    }
};

const onMessage = async message => {
    const { topic = '' } = message;

    switch (topic) {
        case 'settings:updated': {
            const {
                message: { settings }
            } = message;
            await displaySettings(settings);
            break;
        }
        case 'settings:unlock': {
            lockUI(false);
            const {
                message: { settings }
            } = message;
            await displaySettings(settings);
            break;
        }
    }
};

const jumpToHandler = sender => {
    const { target } = sender;
    if (target) {
        const { jumpTo } = target.dataset;
        const jumpTarget = document.getElementById((jumpTo || ' ').substr(1));

        if (jumpTarget) {
            jumpTarget.scrollIntoView();
        }
    }
};

const onLoad = async () => {
    lockUI(false);

    Array.from(
        document.querySelectorAll('button.mailmindr-jump-button')
    ).forEach(btn => {
        btn.addEventListener('click', jumpToHandler);
    });

    Array.from(
        document.querySelectorAll('button.mailmindr-button-web')
    ).forEach(btn => {
        btn.addEventListener('click', webHandler);
    });

    // 
    const port = await browser.runtime.connect({
        name: 'connection:mailmindr-options'
    });

    port.onMessage.addListener(onMessage);

    // 
    document
        .querySelectorAll('button.mailmindr-lock-action')
        .forEach(btn =>
            btn.addEventListener(
                'click',
                async event => await onLockActionButtonClick(port, event)
            )
        );

    const addBtn = document.getElementById('mailmindr-timespan-preset-add');
    addBtn.addEventListener('click', async () => {
        await openTimespanEditor(port, null);
        lockUI(true);
    });

    const editBtn = document.getElementById('mailmindr-timespan-preset-edit');
    editBtn.addEventListener('click', async () => {
        const list = /** @type {HTMLSelectElement} */ (document.getElementById(
            'mailmindr-options_time-preset-list'
        ));
        if (list.value) {
            await openTimespanEditor(port, JSON.parse(list.value));
            lockUI(true);
        }
    });

    const removeBtn = /** @type {HTMLButtonElement} */ (document.getElementById(
        'mailmindr-timespan-preset-remove'
    ));

    removeBtn.addEventListener('click', () => {
        const list = document.getElementById(
            'mailmindr-options_time-preset-list'
        );
        const { value } = /** @type {HTMLSelectElement} */ (list);
        if (value) {
            const preset = JSON.parse(value);
            const { text } = preset;

            if (
                window.confirm(
                    `Delete the preset '${text}'? This cannot be undone.`
                )
            ) {
                messenger.runtime.sendMessage({
                    action: 'preset:remove-timespans',
                    payload: {
                        presets: [preset]
                    }
                });
            }
        }
    });
    removeBtn.disabled = true;

    const list = document.getElementById('mailmindr-options_time-preset-list');
    list.addEventListener('change', sender => {
        const removeBtn = /** @type {HTMLButtonElement} */ (document.getElementById(
            'mailmindr-timespan-preset-remove'
        ));
        removeBtn.disabled = !!!sender.target.value;
    });

    const set = async (settingName, value) => {
        await port.postMessage({
            action: 'settings:set',
            payload: { name: settingName, value }
        });
    };

    // 
    const snoozeTime = document.getElementById('mailmindr-options_snooze-time');
    snoozeTime.addEventListener('change', async sender => {
        const value = sender.target.value;
        await set('snoozeTime', value);
        logger.log(`select snooze time:`, value);
    });

    const defaultTimePreset = document.getElementById(
        'mailmindr-options_default-time-preset'
    );
    defaultTimePreset.addEventListener('change', async sender => {
        const value = JSON.parse(sender.target.value);
        await set('defaultTimePreset', value);
        logger.log(`select default time preset:`, value);
    });

    const defaultActionPreset = document.getElementById(
        'mailmindr-options_default-action-preset'
    );
    defaultActionPreset.addEventListener('change', async sender => {
        const value = JSON.parse(sender.target.value);
        await set('defaultActionPreset', value);
        logger.log(`select default action preset:`, value);
    });

    const defaultRemindMeBeforePicker = document.getElementById(
        'mailmindr-options_default-reminder-preset'
    );
    defaultRemindMeBeforePicker.addEventListener('change', async sender => {
        const value = JSON.parse(sender.target.value);
        logger.log(`select default remindMeMinutesBefore preset:`, value);
        await set('defaultRemindMeMinutesBefore', value);
    });

    // 
    // 
    // 
    // 
    // 
    // 
    // 
    // 

    const iceboxGrid = document.getElementById(
        'mailmindr-options__icebox-grid'
    );
    clearContents(iceboxGrid);

    const accounts = await browser.accounts.list(false);
    for (let account of accounts) {
        const uniqueId = `mailmindr-icebox-account-${account.id}`;
        const label = createIceboxSettingLabel(account.name, uniqueId);

        const identity = account.identities.at(0);
        const accountIdentity = identity?.email || 'localFolders';
        const iceBoxPicker = await createIceboxSettingPicker(
            account,
            uniqueId,
            accountIdentity,
            set
        );

        iceboxGrid.appendChild(label);
        iceboxGrid.appendChild(iceBoxPicker);
    }

    const defaultIceBoxLabel = createIceboxSettingLabel(
        messenger.i18n.getMessage('view.options.default-icebox-folder.label')
    );
    const defaultIceBoxPicker = await createIceboxSettingPicker(
        null,
        'default-0',
        'default',
        set
    );

    iceboxGrid.appendChild(defaultIceBoxLabel);
    iceboxGrid.appendChild(defaultIceBoxPicker);

    translateDocument();

    await port.postMessage({
        action: 'options:get-settings',
        payload: {}
    });
};

// 
// 
// 

const createIceboxSettingLabel = (text, uniqueId) => {
    const label = document.createElement('label');
    label.appendChild(document.createTextNode(text));
    label.htmlFor = uniqueId;

    return label;
};

const createIceboxSettingPicker = async (
    account,
    uniqueId,
    accountIdentity,
    set
) => {
    const selectElement = document.createElement('select');
    const iceBoxPicker = await createFolderPicker(selectElement, account);

    iceBoxPicker.id = uniqueId;
    iceBoxPicker.className = 'mailmindr-js-icebox-picker';
    iceBoxPicker.dataset.accountIdentity = accountIdentity;

    iceBoxPicker.addEventListener('change', async sender => {
        const value = JSON.parse(sender.target.value);
        const accountIdentity = sender.srcElement.dataset.accountIdentity;
        if (accountIdentity === 'default') {
            await set('defaultIceboxFolder', {
                accountIdentity,
                folder: value
            });
        } else {
            await set('setIceBoxFolder', { accountIdentity, folder: value });
        }
    });
    return iceBoxPicker;
};

const findIceboxPickerForIdentity = accountIdentityToken => {
    const iceBoxPickers = document.querySelectorAll('[data-account-identity]');

    const iceBoxPicker = Array.from(iceBoxPickers).find(
        picker => picker?.dataset?.accountIdentity === accountIdentityToken
    );

    return iceBoxPicker;
};

onLoad();
