import { snoozeMindr } from '../../core-utils.mjs.js';
import { createLogger } from '../../logger.mjs.js';
import { createOrUpdateMindr } from './index.mjs.js';

const logger = createLogger('modules/store/actions/snoozeMindrs');

export const snoozeMindrs = (
    mindrGuidList,
    mindrs,
    snoozeTimeMinutes,
    correlationId
) => async (dispatch, getState) => {
    do {
        const guid = mindrGuidList.pop();
        const theMindr = mindrs.find(mindr => mindr.guid === guid);

        if (theMindr) {
            const mindr = structuredClone(theMindr); // { ...theMindr };
            const modifiedMindr = snoozeMindr(mindr, {
                snoozeTimeMinutes,
                correlationId
            });

            await dispatch(createOrUpdateMindr(modifiedMindr));
        } else {
            logger.log(`ugh: ${theMindr}`);
        }
    } while (mindrGuidList.length);
};
