import { createLogger } from '../../logger.mjs.js';
import { createOrUpdateMindr } from './index.mjs.js';

const logger = createLogger('actions/setReplyReceived');

export const setReplyReceived = (
    /** @type {Mindr} */ theMindr,
    /** @type {string} */ replyHeaderMessageId
) => async (dispatch, _getState) => {
    if (theMindr) {
        const mindr = structuredClone(theMindr); // { ...theMindr };
        const modifiedMindr = {
            ...mindr,
            /** @type {Mindr['metaData']} */ metaData: {
                ...mindr.metaData,
                replyHeaderMessageId
            }
        };

        await dispatch(createOrUpdateMindr(modifiedMindr));
    } else {
        logger.error(`mindr is not defined: ${theMindr}`);
    }
};
