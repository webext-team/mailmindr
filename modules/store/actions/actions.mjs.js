import {
    ACTION__CONNECTION_CLOSE,
    ACTION__CONNECTION_OPEN,
    ACTION__DIALOG_CLOSE,
    ACTION__DIALOG_OPEN,
    ACTION__HEARTBEAT,
    ACTION__LOCK_MINDR_FOR_EXECUTION,
    ACTION__MINDR_CREATE_OR_UPDATE,
    ACTION__MINDR_CREATE_OR_UPDATE_DRAFT,
    ACTION__MINDR_REMOVE,
    ACTION__MINDR_REMOVE_DRAFT,
    ACTION__PRESET_TIMESPAN_CREATE,
    ACTION__PRESET_TIMESPAN_REMOVE,
    ACTION__PRESET_TIMESPAN_UPDATE,
    ACTION__SETTINGS_UPDATE,
    ACTION__UNLOCK_MINDR
} from './actionTypes.mjs.js';

export const heartBeat = () => ({
    type: ACTION__HEARTBEAT
});

export const lockMindrForExecution = mindr => ({
    type: ACTION__LOCK_MINDR_FOR_EXECUTION,
    payload: { guid: mindr.guid }
});

export const unlockMindr = mindr => ({
    type: ACTION__UNLOCK_MINDR,
    payload: { guid: mindr.guid }
});

export const createOrUpdateDraft = draft => ({
    type: ACTION__MINDR_CREATE_OR_UPDATE_DRAFT,
    payload: draft
});

export const createOrUpdateMindr = mindr => ({
    type: ACTION__MINDR_CREATE_OR_UPDATE,
    payload: { mindr }
});

export const openDialog = (dialogId, dialogType, details) => ({
    type: ACTION__DIALOG_OPEN,
    payload: { dialogId, dialogType, details }
});

export const closeDialog = dialogId => ({
    type: ACTION__DIALOG_CLOSE,
    payload: { dialogId }
});

export const removeMindr = guid => ({
    type: ACTION__MINDR_REMOVE,
    payload: { guid }
});

export const removeDraft = (/** @type {MindrDraft} */ draft) => ({
    type: ACTION__MINDR_REMOVE_DRAFT,
    payload: { draft }
});

export const connectionOpened = port => ({
    type: ACTION__CONNECTION_OPEN,
    payload: { port }
});

export const connectionClosed = port => ({
    type: ACTION__CONNECTION_CLOSE,
    payload: { port }
});

export const createTimespanPreset = current => ({
    type: ACTION__PRESET_TIMESPAN_CREATE,
    payload: { current }
});

export const updateTimespanPreset = (current, source) => ({
    type: ACTION__PRESET_TIMESPAN_UPDATE,
    payload: { current, source }
});

export const removeTimespanPreset = presets => ({
    type: ACTION__PRESET_TIMESPAN_REMOVE,
    payload: { presets }
});

export const updateSetting = (name, value) => ({
    type: ACTION__SETTINGS_UPDATE,
    payload: { name, value }
});
