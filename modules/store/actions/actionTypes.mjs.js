export const ACTION__HEARTBEAT = 'heartbeat';
export const ACTION__LOCK_MINDR_FOR_EXECUTION = 'state:lock-execution';
export const ACTION__UNLOCK_MINDR = 'state:unlock-execution';
export const ACTION__MINDR_CREATE_OR_UPDATE_DRAFT =
    'mindr:create-or-update-draft';
export const ACTION__MINDR_CREATE_OR_UPDATE = 'mindr:create-or-update';
export const ACTION__MINDR_REMOVE = 'mindr:remove';
export const ACTION__MINDR_REMOVE_DRAFT = 'mindr:remove-draft';
export const ACTION__SETTINGS_UPDATE = 'setting:update';
export const ACTION__DIALOG_OPEN = 'dialog:open';
export const ACTION__DIALOG_CLOSE = 'dialog:close';
export const ACTION__CONNECTION_OPEN = 'connection:open';
export const ACTION__CONNECTION_CLOSE = 'connection:close';
export const ACTION__PRESET_TIMESPAN_CREATE = 'preset:timespan-create';
export const ACTION__PRESET_TIMESPAN_UPDATE = 'preset:timespan-update';
export const ACTION__PRESET_TIMESPAN_REMOVE = 'preset:timespan-remove';
