/** @type {MailmindrState} */
export const initialState = {
    presets: {
        actions: [],
        time: []
    },
    settings: {
        defaultActionPreset: {
            copyMessageTo: null,
            moveMessageTo: null,
            text: null,
            tagWithLabel: null,
            flag: null,
            isSystemAction: false,
            markUnread: false,
            showReminder: false
        },
        defaultIceboxFolder: '',
        defaultTimepreset: {
            days: 0,
            hours: 0,
            minutes: 0,
            isGenerated: true,
            isRelative: false,
            isSelectable: false,
            text: null
        },
        iceboxFolders: [],
        snoozeTime: 15
    },
    mindrs: [],
    active: [],
    overdue: [],
    openDialogs: [],
    openConnections: [],
    __inExecution: []
};
